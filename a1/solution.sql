-- [ACTIVITY]

-- A 
SELECT * FROM artists WHERE name LIKE "%d%";

-- B
SELECT * FROM songs WHERE length < 230;

-- C
SELECT albums.album_title, songs.song_name, songs.length FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- D
SELECT albums.album_title, artists.name FROM artists
	JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%";

-- E
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F
SELECT albums.album_title, songs.song_name FROM albums
	JOIN songs ON albums.id = songs.album_id
	ORDER BY albums.album_title DESC, songs.song_name ASC;