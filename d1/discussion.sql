-- [SECTION] Add new Records

-- Add artists
INSERT INTO artists (name) VALUES("Taylor Swift");
INSERT INTO artists (name) VALUES("Lady Gaga");
INSERT INTO artists (name) VALUES("Justin Bieber");
INSERT INTO artists (name) VALUES("Ariana Grande");
INSERT INTO artists (name) VALUES("Bruno Mars");

-- add albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Fearless",
	"2008-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Red",
	"2012-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"A Star is Born",
	"2018-01-01",
	6
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Born This Way",
	"2011-01-01",
	6
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Purpose",
	"2015-01-01",
	8
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Believe",
	"2012-01-01",
	8
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES(
	"Dangerous Woman",
	"2016-01-01",
	9
);

-- add songs 
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Fearless",
	246,
	"Pop Rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Love Story",
	213,
	"Country Pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Red",
	204,
	"Country",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Born This Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Sorry",
	152,
	"Dancehall-op",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Into You",
	242,
	"EDM",
	9
);

-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 5;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 4;

-- Less than (or equal to)
SELECT * FROM songs WHERE id <= 7;

-- Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 6;

-- Get specific songs via different seach conditions (OR)
SELECT * FROM songs WHERE id = 1 OR song_name = "Red" OR length < 200;

-- get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 5, 6);

-- find partial matches(start search from end of string)
SELECT * FROM songs WHERE song_name LIKE "%e";

-- find partial matches(start search from start of string)
SELECT * FROM songs WHERE song_name LIKE "b%";

-- find partial matches(search entire string)
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- upper/lower
SELECT UPPER(song_name) FROM songs WHERE song_name = UPPER("red");
SELECT LOWER(song_name) FROM songs WHERE song_name = LOWER("RED");

-- sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- limit returned records
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

-- getting distinct records (show all unique values)
SELECT DISTINCT genre FROM songs;

-- count 
SELECT COUNT(*) FROM songs WHERE genre = "Rock";

-- [SECTION] Table Joins

-- combine artists and albums table (INNER JOIN)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- Left Join (show all artist regardless of album status)
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

-- right join (show all artists regardless of album status)
SELECT * FROM albums
	RIGHT JOIN artists ON albums.artist_id = artists.id;

-- Mini activity
-- In a single SELECT command, show all artists with their corresponding albums and songs
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id
	LEFT JOIN songs ON albums.id = songs.album_id;